import path from 'path';

import webpack from 'webpack';
import merge from 'webpack-merge';

import base from './webpack.base';

const fromRoot = path.join.bind(null, __dirname);

export default merge.smart(base, {
  devtool: 'source-map',
  target: 'electron-main',
  entry: fromRoot('app', 'main.dev'),
  output: {
    path: fromRoot('app'),
    filename: 'main.js',
  },
  plugins: [
    new webpack.EnvironmentPlugin({
      NODE_ENV: 'production',
      DEBUG_PROD: 'false'
    })
  ],
  node: {
    __dirname: false,
    __filename: false,
  }
});