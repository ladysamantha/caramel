import path from 'path';

/**
 * Resolves to an absolute path from the root (project) directory
 */
export const fromRoot = path.join.bind(null, __dirname, '..');

export default {
  fromRoot
};