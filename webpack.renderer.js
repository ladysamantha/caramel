// import webpack from 'webpack';
import merge from 'webpack-merge';

import base from './webpack.base';
import { fromRoot } from './utils/index';

export default merge(base, {
  devtool: 'source-map',
  target: 'electron-renderer',
  entry: fromRoot('app', 'index'),
  output: {
    path: fromRoot('app', 'dist'),
    filename: 'renderer.js',
  },
});