// Application entry point
import React from 'react';
import { render } from 'react-dom';

import PackageInfo from './components/PackageInfo';

function Application() {
  return (
    <PackageInfo />
  );
}

render(<Application />, document.getElementById('app'));
