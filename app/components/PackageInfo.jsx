import React from 'react';
import Button from 'material-ui/Button';
import TextField from 'material-ui/TextField';

export default class PackageInfo extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      id: '',
      version: '',
      description: '',
    };
  }

  handleClick() {
    console.log('state: %O', this.state);
  }

  onIdChange(ev) {
    const id = ev.target.value;
    this.setState({
      ...this.state,
      id,
    });
  }

  onVersionChange(ev) {
    const version = ev.target.value;
    this.setState({
      ...this.state,
      version,
    });
  }

  onDescriptionChange(ev) {
    const description = ev.target.value;
    this.setState({
      ...this.state,
      description,
    });
  }

  render() {
    return (
      <form noValidate autoComplete="off">
        <div>
          <TextField
            label="Package ID"
            helperText="ex. onbase-thick"
            value={this.state.id}
            onChange={(ev) => this.onIdChange(ev)} />
        </div>
        <div>
          <TextField
            label="Package Version"
            helperText="ex. 17.0.2.72"
            value={this.state.version}
            onChange={(ev) => this.onVersionChange(ev)} />
        </div>
        <div>
          <TextField
            label="Package Description"
            helperText="..."
            value={this.state.description}
            onChange={(ev) => this.onDescriptionChange(ev)} />
        </div>
        <div>
          <Button raised color="primary" onClick={(ev) => this.handleClick(ev)}>Create Package</Button>
        </div>
      </form>
    );
  }
}
